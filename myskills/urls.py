from django.urls import path

from . import views

app_name = 'myskills'

urlpatterns = [
    path('', views.myskills, name='myskills')
]
